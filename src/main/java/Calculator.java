import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private static final String REGEX = "[a-z]+|[0-9.]+|[-+*/^()]";
    private static final Map<String, Integer> PRIORITIES = createMap();

    private static Map<String, Integer> createMap() {
        Map<String, Integer> map = new HashMap<>();
        map.put("(", 0);
        map.put(")", 0);
        map.put("+", 1);
        map.put("-", 1);
        map.put("*", 2);
        map.put("/", 2);
        map.put("^", 3);
        map.put(null, 4);
        return map;
    }

    private enum Functions {
        SIN, COS, TG, CTG;
    }

    private enum Constants {
        PI(Math.PI), E(Math.E);
        private double value;

        private Constants(final double value) {
            this.value = value;
        }

        public double getValue() {
            return this.value;
        }
    }

    public static double calculate(final String expression) {
        List<String> rpnExpression = convertToRpn(expression);
        Stack<Double> results = new Stack<>();
        // System.out.println(rpnExpression.toString());
        for (String item : rpnExpression) {
            if (isConstant(item)) {
                results.push(Constants.valueOf(item).getValue());
            } else if (isFunction(item)) {
                switch (Functions.valueOf(item.toUpperCase())) {
                case SIN:
                    results.push(Math.sin(results.pop()));
                    break;
                case COS:
                    results.push(Math.cos(results.pop()));
                    break;
                case TG:
                    results.push(Math.tan(results.pop()));
                    break;
                case CTG:
                    results.push(1 / Math.tan(results.pop()));
                    break;
                default:
                    throw new IllegalArgumentException("No such function");
                }
            } else if (isOperator(item)) {
                double tmp;
                switch (item) {
                case "+":
                    tmp = results.pop();
                    results.push(results.pop() + tmp);
                    break;
                case "-":
                    tmp = results.pop();
                    results.push(results.pop() - tmp);
                    break;
                case "*":
                    tmp = results.pop();
                    results.push(results.pop() * tmp);
                    break;
                case "/":
                    tmp = results.pop();
                    results.push(results.pop() / tmp);
                    break;
                case "^":
                    tmp = results.pop();
                    results.push(Math.pow(results.pop(), tmp));
                    break;
                default:
                    throw new IllegalArgumentException("No such operator");
                }
            } else {
                results.push(Double.parseDouble(item));
            }
        }
        return results.peek();
    }

    private static List<String> convertToRpn(final String expression) {
        List<String> result = new ArrayList<>();
        Stack<String> operations = new Stack<>();
        Pattern p = Pattern.compile(REGEX);
        Matcher m = p.matcher(expression);
        while (m.find()) {
            String temp = m.group();
            if (isConstant(temp)) {
                result.add(temp);
            } else if (isFunction(temp) | isOpeningBracket(temp)) {
                operations.push(temp);
            } else if (isClosingBracket(temp)) {
                String operaton;
                do {
                    operaton = operations.pop();
                    if (!isOpeningBracket(operaton)) {
                        result.add(operaton);
                    }
                } while (!isOpeningBracket(operaton));
            } else if (isOperator(temp)) {
                while (!operations.isEmpty() && getPriority(
                        temp) <= getPriority(operations.peek())) {
                    result.add(operations.pop());
                }
                operations.push(temp);
            } else {
                result.add(temp);
            }
        }
        while (!operations.isEmpty()) {
            result.add(operations.pop());
        }
        return result;
    }

    private static int getPriority(final String temp) {
        String s = temp;
        if (!PRIORITIES.containsKey(s)) {
            s = null;
        }
        return PRIORITIES.get(s);
    }

    private static boolean isClosingBracket(final String temp) {
        return ")".equals(temp);
    }

    private static boolean isOpeningBracket(final String temp) {
        return "(".equals(temp);
    }

    private static boolean isFunction(final String temp) {
        for (Functions c : Functions.values()) {
            if (c.name().toLowerCase().equals(temp)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isConstant(final String temp) {
        for (Constants c : Constants.values()) {
            if (c.name().toLowerCase().equals(temp)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isOperator(final String temp) {
        return PRIORITIES.containsKey(temp);
    }
}
